import React, { useState } from 'react';
import { HeaderWrap, RightHead, LeftHead } from './header.style';
import { Button } from 'antd';
import { CloudUploadOutlined } from '@ant-design/icons';
import UploadModal from './modal';

const HeaderComponent = () => {
  const [visible, setVisible] = useState<boolean>(false);

  const hideModal = () => setVisible(false);

  const showModal = () => setVisible(true);

  return (
    <HeaderWrap>
      <LeftHead>
        Photos
      </LeftHead>
      <RightHead>
        <Button onClick={showModal} type="text" icon={<CloudUploadOutlined />}>Upload</Button>
      </RightHead>
      <UploadModal visible={visible} onCancel={hideModal} />
    </HeaderWrap>
  );
}

export default HeaderComponent;