import styled from 'styled-components';

export const HeaderWrap = styled.div`
  display: flex;
  width: 100%;
`;

export const RightHead = styled.div`
  width: 50%;
  text-align: right;
`;

export const LeftHead = styled.div`
  width: 50%;
`;

export const UploadBody = styled.div``