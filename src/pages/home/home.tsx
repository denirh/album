import React, { useEffect, useState } from 'react';
import HeaderComponent from '../../components/header';
import { Wrapper } from './home.style';
import { Card, Row, Col, notification } from 'antd';
const { Meta } = Card;

const HomePage = () => {
  const [limit, setLimit] = useState<number>();
  const [data, setData] = useState<any[]>();

  const getDatas = async (params: any) => {
    const rawResponse = await fetch('http://localhost:8888/photos/list', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(params)
    });
    const content: any = await rawResponse.json();

    return content;
  }

  useEffect(() => {
    const params: any = {
      skip: 0,
      limit: 5
    };

      (async () => {
        const contents: any = await getDatas(params);

        setLimit(contents.limit);
        setData(contents.documents);
      })();
  },[]);

  const loadMore = async () => {
    const params: any = {
      skip: 0,
      limit: limit ? limit+5 : 5
    };

    const newData: any = await getDatas(params);

    setLimit(newData.limit);
    setData(newData.documents);
  }

  const deleteImage = async (params: any) => {
    let param: any = {
      album: params.album,
      documents: params.name
    };
    param = [param];
    const rawResponse = await fetch('http://localhost:8888/photos', {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(param)
    });
    const content: any = await rawResponse.json();
     if (content) {
       notification.success({
         duration: 5,
         message: 'Success',
         description: 'Successfully delete images'
       })
      const params: any = {
        skip: 0,
        limit: 5
      };
      const contents: any = await getDatas(params);

      setLimit(contents.limit);
      setData(contents.documents);
     }
  }

  return (
    <Wrapper>
      <HeaderComponent />
      <Row gutter={16}>
        {data ? data.map((item: any) => {
          return (
            <Col span={6}>
              <Card
                style={{ width: 300 }}
                cover={
                  <img
                    alt={item.album}
                    src={item.raw}
                    width={250}
                    height={250}
                  />
                }
                actions={[
                  <span onClick={() => {
                    deleteImage(item);
                  }}>Hapus</span>,
                ]}
                >
                <Meta
                  title={item.album}
                />
              </Card>
            </Col>
          )
        }) : []}
      </Row>
      <div style={{
        cursor: 'pointer',
        textAlign: 'center',
        marginTop: '5em'
      }} onClick={loadMore}>Load more...</div>
    </Wrapper>
  );
}

export default HomePage;