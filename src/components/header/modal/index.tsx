import React, { useState } from 'react';
import { Modal, Upload, message, Select, Button, notification } from 'antd';
import { LoadingOutlined, PlusOutlined, CloudUploadOutlined } from '@ant-design/icons';

const { Option } = Select;

interface IProps {
  visible: any;
  onCancel: () => void;
}

function getBase64(img: any, callback: any) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file: any) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
}

const UploadModal = (props: IProps) => {
  const [album, setAlbum] = useState<string>();
  const [isEmpty, setIsEmpty] = useState<boolean>(true);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [imageUrl, setImageUrl] = useState<string>();
  const [file, setFile] = useState<File>();
  const { visible, onCancel } = props;

  const handleChange = (info: any) => {
    if (info.file.status === 'uploading') {
      setIsLoading(true);
      return;
    }
    if (info.file.status === 'done') {
      setFile(info.file.originFileObj);
      setIsEmpty(false);
      getBase64(info.file.originFileObj, (imageUrl: any) => {
        setImageUrl(imageUrl);
        setIsLoading(false);
      });
    }
  };

  const uploadButton = (
    <div>
      {isLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Click or drag and drop to upload</div>
    </div>
  );

  const handleAlbum = (value: string) => setAlbum(value);

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("album", album ? album : '');
    formData.append("documents", file ? file : '', file ? file.name : '');

    const requestOptions: any = {
      method: 'PUT',
      body: formData,
      redirect: 'follow'
    };

    fetch("http://localhost:8888/photos", requestOptions)
      .then((response: any) => response.text())
      .then((result: any) => {
        if (result) {
          notification.success({
            duration: 5,
            message: 'Sukses',
            description: 'Berhasil upload gambar'
          });
        }
      })
      .catch((error: any) => {
        notification.error({
          duration: 5,
          message: 'Error',
          description: 'Gagal upload gambar'
        })
      });
  }

  return (
    <>
      <Modal
        visible={visible}
        onCancel={onCancel}
        footer={null}
        title="Upload Photos"
      >
        <Upload
          name="avatar"
          listType="picture-card"
          className="avatar-uploader"
          showUploadList={false}
          action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
          beforeUpload={beforeUpload}
          onChange={handleChange}
        >
          {imageUrl ? <img src={imageUrl} alt="avatar" style={{ width: '100%' }} /> : uploadButton}
        </Upload>
        
        {isEmpty && (
          <div style={{ textAlign: 'center', padding: '3em'}}>
            No files selected...
          </div>
        )}
        <div style={{ width: '100%', display: 'flex', marginTop: '1em' }}>
          <div style={{ width: '50%' }}>
            <Select
              placeholder="Select album"
              style={{ width: 140 }}
              bordered={false}
              onChange={handleAlbum}
            >
              <Option value="travel">Travel</Option>
              <Option value="personal">Personal</Option>
              <Option value="food">Food</Option>
              <Option value="nature">Nature</Option>
              <Option value="other">Other</Option>
            </Select>
          </div>
          <Button
            style={{ textAlign: 'right', width: '50%' }}
            type="text"
            icon={<CloudUploadOutlined />}
            disabled={!album ? true : false}
            onClick={handleUpload}
          >
            Upload
          </Button>
        </div>
      </Modal>
    </>
  );
}

export default UploadModal;